package math_ricky

func Fact(num int) (int) {
	idx := 1
	result := 1
	for idx <= num {
		result = result * idx
		idx += 1
	}
	return result
}
