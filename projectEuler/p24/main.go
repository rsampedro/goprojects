package main

import (
	"math_ricky"
	"strconv"
	"strings"
	"fmt"
)

// Find the 10^6 permutation of the digits [0,1,2,3,4,5,6,7,8,9], being the permutations sorted by their value

func main() {
	var permutation []int
	list_nums := []int{0,1,2,3,4,5,6,7,8,9}
	rem_nums := []int{0,1,2,3,4,5,6,7,8,9}
	
	counter := 1000000 -1
	for len(permutation)< len(list_nums){
		if counter == 0{
			ammount_scroll := 0
			permutation = append(permutation, rem_nums[ammount_scroll])
			rem_nums = append(rem_nums[:ammount_scroll], rem_nums[ammount_scroll+1:]...)
		}else{
			ammount_scroll := counter/math_ricky.Fact(len(rem_nums)-1)
			permutation = append(permutation, rem_nums[ammount_scroll])
			rem_nums = append(rem_nums[:ammount_scroll], rem_nums[ammount_scroll+1:]...)
			counter = counter - (math_ricky.Fact(len(rem_nums))*ammount_scroll)
		}

	}

	perm_str := []string{}
	for i := range permutation{
		number := permutation[i]
		text := strconv.Itoa(number)
		perm_str = append(perm_str, text)
	}
	result := strings.Join(perm_str, "")
	fmt.Println("Result: "+result)
}
